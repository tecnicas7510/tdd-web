export const ROUTES = {
  LOGIN: '/signIn',
  REGISTER: '/signUp',
  CONTENT: '/content',
  EDIT_PROFILE: '/editProfile',
  EVAL: '/eval'
};
