import firebase from  'firebase';
import { removeValue, setValue, getValue } from "./services/LocalStorageService";
import { post } from './utils/requests';


const config = {
    apiKey: "AIzaSyBnxiDEgVqgKKx55HpIAP30PJtzrNSE7dc",
    authDomain: "tdd-server-e8da1.firebaseapp.com",
    projectId: "tdd-server-e8da1",
    storageBucket: "tdd-server-e8da1.appspot.com",
    messagingSenderId: "170818433827",
    appId: "1:170818433827:web:6dff9fc8f117f0fb455296",
    measurementId: "G-V37LKMSC8J"
};
firebase.initializeApp(config);


firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      const localUser = getValue("user");
      if (user.displayName){
        const completeName = user.displayName.split(" ");
        const currentUser = {
          name: completeName[0],
          surname: completeName[2] ? completeName[1] + " " + completeName [2] : completeName[1],
          email: user.email,
          profilePic: user.photoURL,
        };
        post({ body: currentUser, apiRoute: 'api/users/signIn/google'})
          .then(res => {
            setValue("user", res.data);
          })
          .catch(err => {
            removeValue("user")
            post({ body: currentUser, apiRoute: 'api/users/signUp/google'})
              .then(res => {
                setValue("user", res.data);
              })
              .catch(err => {
                removeValue("user")
                console.log(err)
            })
          })
      }
    } else {
        removeValue("user")
        console.log("Se cerró la sesión de google");
    }
  });

export default firebase;
