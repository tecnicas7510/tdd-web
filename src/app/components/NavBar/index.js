import React, {useState} from 'react';
import {useHistory} from "react-router-dom";
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Upload from "../Upload";
import {Grid} from "@material-ui/core";
import styles from "./styles.module.scss"
import {ROUTES} from '../../../constants/routes';
import firebase from "../../../Firebase";
import {getValue, removeValue} from '../../../services/LocalStorageService';
import {post} from '../../../utils/requests'
import Button from "@material-ui/core/Button";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function MenuAppBar({onRefresh, path}) {
  const history = useHistory();
  const classes = useStyles();
  const [auth, setAuth] = useState(true);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleChange = (event) => {
    setAuth(event.target.checked);
  };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleEditProfile = () => history.push(ROUTES.EDIT_PROFILE);
  const handleLogout = () => {
    const user = getValue("user")
    if (user.provider === "google") {
      firebase.auth().signOut().then(() => {
        post({headers: {'x-auth-token': user.accessToken}, apiRoute: 'api/users/signOut'})
            .then(res => {
              removeValue("user");
              history.replace(ROUTES.LOGIN)
            })
            .catch(err => console.log(err))
      })
          .catch((error) => {
            console.log("Se cerró mal sesion")
          });
    } else {
      post({body: {'_id': user._id}, headers: {'x-auth-token': user.accessToken}, apiRoute: 'api/users/signOut'})
          .then(res => {
            removeValue("user");
            history.replace(ROUTES.LOGIN)
          })
          .catch(err => console.log(err))
    }
    removeValue("user");
    history.replace(ROUTES.LOGIN)
  }

  return (
      <div className={classes.root}>
        {/* <FormGroup>
        <FormControlLabel
          control={<Switch checked={auth} onChange={handleChange} aria-label="login switch" />}
          label={auth ? 'Logout' : 'Login'}
        />
      </FormGroup> */}
        <AppBar position="static">
          <Toolbar>
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
              <MenuIcon/>
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              TDD-Drive
            </Typography>
            {auth && (
                <div>
                  <Grid container className={styles.uploadButton}>
                    <div className={styles.evalButtonBox}>
                      <Button variant="contained" onClick={() => {
                        history.push(ROUTES.EVAL)
                      }}>
                        Go to Eval
                      </Button>
                    </div>
                    {path !== "/shared/" &&
                      <Grid item>
                        <Upload onRefresh={() => onRefresh()} path={path}/>
                      </Grid>
                    }
                    <Grid item>
                      <IconButton
                          aria-label="account of current user"
                          aria-controls="menu-appbar"
                          aria-haspopup="true"
                          onClick={handleMenu}
                          color="inherit"
                      >
                        <AccountCircle/>
                      </IconButton>
                      <Menu
                          id="menu-appbar"
                          anchorEl={anchorEl}
                          anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                          }}
                          keepMounted
                          transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                          }}
                          open={open}
                          onClose={handleClose}
                      >
                        <MenuItem onClick={handleEditProfile}>Editar mi perfil</MenuItem>
                        <MenuItem onClick={handleLogout}>Cerrar sesión</MenuItem>
                      </Menu>
                    </Grid>
                  </Grid>
                </div>
            )}
          </Toolbar>
        </AppBar>
      </div>
  );
}
