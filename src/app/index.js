import React, {Suspense} from 'react';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';

import Login from './screens/Login';
import Register from './screens/Register';
import Content from "./screens/Content";
import firebase from "../Firebase.js";
import {ROUTES} from '../constants/routes';
import Eval from "./components/Eval";


const App = () => (
    <Router>
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <Route
              exact
              path="/"
              render={() => {
                return (
                    firebase.auth().currentUser ?
                        <Redirect to={ROUTES.CONTENT}/> :
                        <Redirect to={ROUTES.LOGIN}/>
                )
              }}
          />
          <Route exact path={ROUTES.REGISTER} component={Register}/>
          <Route exact path={ROUTES.LOGIN} component={Login}/>
          <Route exact path={ROUTES.CONTENT} component={Content}/>
          <Route exact path={ROUTES.EDIT_PROFILE} component={Register}/>
          <Route exact path={ROUTES.EVAL} component={Eval}/>
        </Switch>
      </Suspense>
    </Router>
);

export default App;

