import axios from 'axios'

const url = "https://api.grupo0.tecnicasdedisenio.com.ar/"

const TIMEOUT = 15000;

export const get = ({
	params,
	headers,
	apiRoute
}) => {

	const instance = axios.create({
		baseURL: url,
		timeout: TIMEOUT,
	});

	return instance.get(apiRoute, {
		...params && { params },
		...headers && { headers }
	});
};

export const post = ({ body, apiRoute, headers }) => {

	const instance = axios.create({
		baseURL: url,
		timeout: TIMEOUT,
	});

	return instance.post(apiRoute, body, {...headers && { headers }});
};

export const patch = ({ body, apiRoute, headers }) => {

	const instance = axios.create({
		baseURL: url,
		timeout: TIMEOUT
	});

	return instance.patch(apiRoute, body, {...headers && { headers }});
};

export const deleteMethod = ({
	params,
	headers,
	apiRoute
}) => {

	const instance = axios.create({
		baseURL: url,
		timeout: TIMEOUT,
	});
	
	return instance.delete(apiRoute, {
		...params && { params },
		...headers && { headers }
	});
};
